# palbot
palworldのサーバーを開け閉めするDiscord bot

# 使い方
デバッグしてないんで念のためワールドデータのバックアップ取ってから使ってください。

## 前提
Rustの最新版が導入されている

RCONが有効化されている(RCONポートの開放は不要)

AdminPasswordが設定されている

## 導入
PalServerを稼働させているのと同じPCにダウンロード->
config_template.yamlをコピーして同じ階層に配置->
config_template.yaml→config.yamlに名前を変更->
config.yamlを設定する->
`cargo run`して起動

# コマンド
`!pal start`: サーバーを起動

`!pal stop`: サーバーを停止

# 免責事項
本ソフトウェアを使用して発生した損害等に対して開発者は一切の責任を負いません。

