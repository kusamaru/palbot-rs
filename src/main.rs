use std::process::Child;
use std::process::Command;
use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;

use once_cell::sync::Lazy;
use rcon::Connection;
use serenity::all::Message;
use serenity::all::Ready;
use serenity::async_trait;
use serenity::framework::standard::macros::command;
use serenity::framework::standard::macros::group;
use serenity::framework::standard::CommandResult;
use serenity::framework::standard::Configuration;
use serenity::framework::StandardFramework;
use serenity::gateway::ActivityData;
use serenity::prelude::*;
use tokio::net::TcpStream;


static CHILD_TASK: Lazy<Arc<RwLock<Option<Child>>>> = Lazy::new(|| Arc::new(RwLock::new(None)));

static RAW_CONFIG: Lazy<String> = Lazy::new(|| std::fs::read_to_string("./config.yaml").unwrap());

static TOKEN: Lazy<String> = Lazy::new(|| get_config("token").expect("couldn't find token"));
static PALSERVER_PATH: Lazy<String> = Lazy::new(|| get_config("palserver_path").expect("couldn't find palserver_path"));
static PORT: Lazy<String> = Lazy::new(|| get_config("port").expect("couldn't find port"));
static RCON_PORT: Lazy<String> = Lazy::new(|| get_config("rcon_port").expect("couldn't find rcon_port"));
static RCON_PASSWORD: Lazy<String> = Lazy::new(|| get_config("rcon_password").expect("couldn't find rcon_password"));
fn get_config(idx: &str) -> Option<String> {
    RAW_CONFIG
        .split('\n')
        .find(|s| s.starts_with(&format!("{idx}:")))
        .and_then(|s| s.splitn(2, ":").last().map(|v| v.trim()))
        .map(|s| s.to_string())
}

enum PalbotStatus {
    Running,
    Stopped,
    Unknown,
}

async fn set_status(ctx: &Context, status: PalbotStatus) {
    let text = match status {
        PalbotStatus::Running => "✅サーバー稼働中✅",
        PalbotStatus::Stopped => "⏸サーバー休止中⏸",
        PalbotStatus::Unknown => "❓サーバーの状態不明❓",
    };
    ctx.set_activity(Some(ActivityData::playing(text)));
}

async fn update_status(ctx: &Context) {
    let is_running = CHILD_TASK.read().await.is_some();
    match is_running {
        true => { set_status(ctx, PalbotStatus::Running).await; }
        false => { set_status(ctx, PalbotStatus::Stopped).await; }
    }
}

#[command]
async fn start(ctx: &Context, msg: &Message) -> CommandResult {
    if CHILD_TASK.read().await.is_some() {
        let _ = &msg.channel_id.say(&ctx.http, "もう起動してるよ").await;
        return Ok(())
    }
    println!("booting");
    let child = Command::new(&*PALSERVER_PATH)
        .arg("-useperfthread")
        .arg("-NoAsyncLoadingThread")
        .arg("-UseMultithreadForDS")
        .arg(format!("-publicport={}", &*PORT))
        .arg(format!("-port={}", &*PORT))
        .spawn()
        .expect("server error");
    { // write part
        CHILD_TASK.write().await.replace(child);
    }
    println!("boot done");
    let _ = &msg.channel_id.say(&ctx.http, "起動したよ").await;
    update_status(ctx).await;
    Ok(())
}
#[command]
async fn stop(ctx: &Context, msg: &Message) -> CommandResult {
    if CHILD_TASK.read().await.is_none() {
        let _ = &msg.channel_id.say(&ctx.http, "サーバー起動してないです…").await;
        return Ok(());
    }
    println!("stopping");
    let _ = &msg.channel_id.say(&ctx.http, "サーバーを停止中…").await;
    let addr = format!("127.0.0.1:{}", &*RCON_PORT);
    let Ok(mut conn) = <Connection<TcpStream>>::builder()
        .connect(addr, &*RCON_PASSWORD)
        .await
    else {
        println!("Rcon connect error");
        return Ok(());
    };
    match conn.cmd("shutdown 15").await {
        Ok(_) => {},
        Err(e) if format!("{e:?}").contains("UnexpectedEof") => {},
        Err(e) => {
            println!("Error: {e:?}");
            let _ = &msg.channel_id.say(&ctx.http, "とめられなかったんで手動で/shutdownしてください").await;
            return Ok(())
        }
    };
    let _ = &msg.channel_id.say(&ctx.http, "停止したよ").await;
    sleep(Duration::from_secs(3));
    { // write part
        let mut guard = CHILD_TASK.write().await;
        if guard.is_some() {
            let mut child = guard.take().unwrap();
            match child.try_wait() {
                Ok(Some(_)) => println!("process stopped"),
                Ok(None) => {
                    child.kill().expect("server stopping error");
                    println!("task killed");
                },
                Err(e) => {
                    println!("Error: {e:?}");
                    return Ok(())
                }
            }
        }
    }
    update_status(ctx).await;
    Ok(())
}

#[command]
async fn status(ctx: &Context, msg: &Message) -> CommandResult {
    let lock = CHILD_TASK.read().await;
    match lock.is_some() {
        true => {
            let _ = &msg.channel_id.say(&ctx.http, "`✅サーバーは起動してます`").await;
        },
        false => {
            let _ = &msg.channel_id.say(&ctx.http, "`⏸サーバーは停止してます`").await;
        }
    }
    Ok(())
}

struct Handler;
#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, _: Ready) {
        update_status(&ctx).await;
        println!("Palbot is ready.");
    }
}

#[group]
#[commands(start, stop, status)]
struct General;

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new().group(&GENERAL_GROUP);
    framework.configure(Configuration::new().with_whitespace(true).prefix("!pal"));

    let intents = 
        GatewayIntents::non_privileged() | 
        GatewayIntents::MESSAGE_CONTENT;

    let mut client = Client::builder(&*TOKEN, intents)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("bad client error");

    println!("server path: {}", &*PALSERVER_PATH);
    println!("server port: {}", &*PORT);
    println!("rcon port:   {}", &*RCON_PORT);
    println!("rcon pass:   {}", &*RCON_PASSWORD);

    tokio::spawn(async move {
        let _ = client.start().await.map_err(|e| println!("{e:?}"));
    });

    tokio::signal::ctrl_c().await.expect("");
    println!("Ctrl+C");
}
